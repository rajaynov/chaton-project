const db = require('../dbconnect.js');
const jwt = require('../jwt.utils');
const bcrypt = require('bcrypt');

const User = function(user){
    this.name = user.name;
    this.pseudo = user.pseudo;
    this.email = user.email;
    this.pwd = user.pwd;
};

exports.getAllUsers = ((req, res ) => {
    db.query("SELECT * FROM USERS", function (err, result) {
        if (err)
            res.status(500).json(err);
        else
            res.status(200).json(result);
    })
});

exports.register = ((newUser, res) => {
    const {name, pseudo, email, password} = newUser.query.user;
    const cryptedpassword = bcrypt.hashSync(password, 4);
    const statement = "INSERT INTO USERS(name, pseudo, email, password) VALUES (?, ?, ? ,?);";
    db.query(statement, [name, pseudo, email, cryptedpassword],function(err, result){
        if(err)
            res.status(500).json(err);
        else
            res.status(200).json(result);
    });
});

exports.login = ((user, res) => {
    const {pseudo, password} = user.query.user;
    db.query("SELECT pseudo, password FROM USERS WHERE ?;", {pseudo}, function (err, result) {
        if (err)
            res.status(500).json("err");
        else if (JSON.stringify(result) === '[]')
            res.status(404).json("User not found");
        else {
            const usr = JSON.parse(JSON.stringify(result));
            bcrypt.compare(password, usr[0].password, function(errBCrypt, resBCrypt){
                if (errBCrypt)
                    res.status(500).json("err bcrypt");
                else if (resBCrypt == false)
                    res.status(403).json("Invalid password");
                else {
                    res.status(200).json({
                        'pseudo': usr[0].pseudo,
                        'token': jwt.generateTokenForUser(usr[0])
                    });
                }
            })
        }
    })
});

exports.getUser = ((pseudo, res) =>{
    db.query("SELECT * FROM USERS WHERE pseudo LIKE ?" , pseudo.params.pseudo, function (err, result) {
        if (err)
            res.status(500).json(err);
        else if (JSON.stringify(result) === '[]')
            res.status(404).json(result);
        else
            res.status(200).json(result);

    });
});

exports.updateUser = ((user, res) =>{
    const {name, email, password} = user.query.user;
    const pseudo = user.params.pseudo;
    const statement = "UPDATE USERS SET ? WHERE pseudo = ?";
    db.query(statement, [{name, pseudo, email, password}, pseudo], function (err, result) {
        if(err)
            res.status(500).json(err);
        else
            res.status(200).json(result);
    });
});

exports.deleteUser = ((user, res) =>{
    const pseudo = user.params.pseudo;
    db.query("DELETE FROM USERS WHERE ?", {pseudo}, function (err, result){
        if(err)
            res.status(500).json(err);
        else
            res.status(200).json(result);
    });
});
