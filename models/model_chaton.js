const db = require('../dbconnect');

exports.getAllchatons = ((req, res) =>{
    db.query("SELECT * FROM CHATONS", function (err, result) {
        if (err)
            res.status(500).json(err);
        else
            res.status(200).json(result);
    })
});

exports.getChaton = ((chaton, res) =>{
    const id = chaton.params.id;
    db.query('SELECT * FROM CHATONS WHERE id = ?', id, function (err, result) {
        if(err)
            res.status(500).json(err);
        else if (JSON.stringify(result) === '[]')
            res.status(404).json("Chaton not found");
        else
            res.status(200).json(result);
    })
});

exports.createChaton = ((chaton, res) => {
    const {color, age, race, photo} = chaton.query.chaton;
    const statement = "INSERT INTO CHATONS(color, age, race, photo) VALUES (?, ?, ?, ?)";
    db.query(statement, [color, age, race, photo], function (err, result) {
       if (err)
           res.status(500).json(err);
       else
           res.status(204).json('Insertion complete');
    })
});

exports.updateChaton = ((chaton, res) => {
    const {color, age, race, photo} = chaton.query.chaton;
    const id = chaton.params.id;
    const statement = "UPDATE CHATONS SET ? WHERE id = ?";
    db.query(statement, [{color, age, race, photo} , id], function (err, result) {
        if (err)
            res.status(500).json(err);
        else if (result.affectedRows == 0)
            res.status(404).json("Chaton not found");
        else
            res.status(204).json("Modification complete");
    })
});

exports.deleteChaton = ((chaton, res) => {
    const id = chaton.params.id;
    db.query("DELETE FROM CHATONS WHERE id = ?", id, function(err, result) {
        if (err)
            res.status(500).json(err);
        else if (result.affectedRows == 0)
            res.status(404).json("Chaton not found");
        else
            res.status(204).json("Chaton delete");
    })
});
