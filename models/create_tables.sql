DROP TABLE IF EXISTS USERS;
DROP TABLE IF EXISTS CHATONS;

CREATE TABLE USERS (
                     name VARCHAR(64),
                     pseudo VARCHAR (64),
                     email VARCHAR (180),
                     password VARCHAR (255),
                     PRIMARY KEY(pseudo)
);

CREATE TABLE CHATONS (
                       id INTEGER AUTO_INCREMENT,
                       color ENUM("blanc", "roux", "noir"),
                       age DATE,
                       race ENUM("siamois", "sphynx", "europeen"),
                       photo LONGTEXT,
                       PRIMARY KEY (id)
);