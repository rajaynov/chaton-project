const jwt = require('jsonwebtoken');

const JWT_SECRET_KEY = '88QRG6uVCb';

module.exports  = {
    generateTokenForUser: function(user) {
        return jwt.sign({
            pseudo: user.pseudo,
        },
        JWT_SECRET_KEY,
        {
            expiresIn: '1h'
        })
    }
}