const express = require('express');
const  app = express();
const port = process.env.PORT || 3000;


const bodyparser = require('body-parser');

var route = require('./routes/approutes')

route(app);
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());

app.listen(port);
console.log('Application running on ' + port +' port');


