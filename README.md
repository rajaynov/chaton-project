# Chaton project

This repository contains the DevOps remedial project.

## Technologies
The API is developped on NodeJS, routed by Express.
The database is hosted in MySQL

## Database
We have two tables in this project : 
    
    - USERS
    - CHATON
    
## Design
The main file is server.js which calls the routes on the approutes.js
when a route is given it execute the function on the models files.

## Functionalities

This project is able to response to Front requests. It allows to users to register and login using JWT Token.
We can add, update or delete a chaton.

    
## How to run the project
Run the command : 

    npm start
 
And use it with a Front end application or with Postman