const mysql = require('mysql')
const connection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    database:'chaton',
    port:'8889'
});

connection.connect((err )=>{
   if(err) throw err;
   console.log('Successful connection');
});

module.exports = connection;