const user = require('../models/model_user');
const chaton = require('../models/model_chaton')

module.exports = function(app) {
    app.route('/users')
        .get(user.getAllUsers)

    app.route('/users/register')
        .post(user.register);

    app.route('/users/login')
        .post(user.login);

    app.route('/users/:pseudo')
        .get(user.getUser)
        .put(user.updateUser)
        .delete(user.deleteUser);

    app.route('/chatons')
        .get(chaton.getAllchatons)
        .post(chaton.createChaton);

    app.route('/chatons/:id')
        .get(chaton.getChaton)
        .put(chaton.updateChaton)
        .delete(chaton.deleteChaton);

};
